from django.contrib import admin

# Register your models here.
from app.models import Question, Comment


class QuestionAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Question, QuestionAdmin)
admin.site.register(Comment)
