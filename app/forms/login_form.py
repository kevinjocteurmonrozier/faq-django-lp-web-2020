from django import forms
from django.contrib.auth.models import User
from django.forms import widgets


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30, label='Nom d\'utilisateur',
                                widget = forms.TextInput(
                                attrs={'class': "form-control mb-3", 'placeholder': "Nom d'utilisateur"}), )
    password = forms.CharField(max_length=200, min_length=4, label='Mot de passe',
                               widget=widgets.PasswordInput(attrs={'class': "form-control mb-3",
                                                                   'placeholder': "Mot de passe"}, ))

    class Meta:
        model = User
        fields = ('username', 'password', )
