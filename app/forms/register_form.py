from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import widgets


class RegisterForm(UserCreationForm):
    username = forms.CharField(max_length=30, label='Nom d\'utilisateur',
                               widget=forms.TextInput(
                                   attrs={'class': "form-control mb-3", 'placeholder': "Nom d'utilisateur"}), )
    email = forms.EmailField(max_length=200, label='E-mail',
                             widget=forms.TextInput(attrs={'class': "form-control mb-3", 'placeholder': "Email"}), )
    password1 = forms.CharField(max_length=200, min_length=4, label='Mot de passe',
                                widget=widgets.PasswordInput(attrs={'class': "form-control mb-3",
                                                                    'placeholder': "Mot de passe"}, ))
    password2 = forms.CharField(max_length=200, min_length=4, label='Répétez le mot de passe',
                                widget=widgets.PasswordInput(attrs={'class': "form-control mb-3",
                                                                    'placeholder': "Répétez le mot de passe"}, ))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )


