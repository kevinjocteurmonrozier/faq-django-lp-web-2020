from django import forms
from app.models import Comment


class AddCommentForm(forms.ModelForm):
    description = forms.CharField(max_length=2000, label='Description',
                                  widget=forms.Textarea(attrs={'class': "form-control mb-3",
                                                               'placeholder': "Commentaire",
                                                               'rows': "3"}),)

    class Meta:
        model = Comment
        fields = ('description',)

    def save(self, **kwargs):
        user = kwargs.pop('user')
        question = kwargs.pop('question')
        instance = super(AddCommentForm, self).save(**kwargs)
        instance.user = user
        instance.question = question
        instance.save()
        return instance
