from django import forms
from app.models import Question


class AddQuestionForm(forms.ModelForm):
    title = forms.CharField(max_length=50, label='Titre',
                            widget=forms.TextInput(attrs={'class': "form-control mb-3",
                                                          'placeholder': "Titre de la question"}), )
    description = forms.CharField(max_length=2000, label='Description',
                                   widget=forms.Textarea(attrs={'class': "form-control mb-3",
                                                                 'placeholder': "Description de la question"}), )

    class Meta:
        model = Question
        fields = ('title', 'description',)

    def save(self, **kwargs):
        user = kwargs.pop('user')
        instance = super(AddQuestionForm, self).save(**kwargs)
        instance.user = user
        instance.save()
        return instance
