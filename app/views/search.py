from django.db.models import Q
from django.views.generic import ListView

from app.models import Question


class QuestionSearchView(ListView):
    template_name = 'index.html'
    model = Question
    paginate_by = 3

    def get_queryset(self):
        search = self.request.GET['search']
        return Question.objects.filter(Q(title__icontains=search) | Q(user__username__icontains=search))

