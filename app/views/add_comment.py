from django.contrib.auth import authenticate
from django.shortcuts import redirect, render, get_object_or_404

from app.forms.comment_form import AddCommentForm
from app.models import Question


def add_comment_view(request, slug):
    if request.method == 'POST':
        form = AddCommentForm(request.POST)
        if form.is_valid():
            question = get_object_or_404(Question, slug=slug)
            post = form.save(user=request.user, question=question, commit=False)
            post.save()
            description = form.cleaned_data.get('description')
            return redirect('app_question_detail', slug=slug)
    else:
        form = AddCommentForm()
    return render(request, 'question_detail.html', {'form': form})
