from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render

from app.forms.login_form import LoginForm


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('app_home')
            else:
                return redirect('app_login')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})
