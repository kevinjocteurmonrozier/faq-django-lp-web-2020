from django.views.generic import DetailView

from django.shortcuts import redirect, render


from app.forms.comment_form import AddCommentForm
from app.models import Question


# TODO QUESTIONS PAR DATES, PAR HOT, PAR ALL TIME LIKES


class QuestionsDetailView(DetailView):
    template_name = 'question_detail.html'
    model = Question

    def get_context_data(self, **kwargs):
        context = super(QuestionsDetailView, self).get_context_data(**kwargs)
        context['form'] = AddCommentForm()
        return context
