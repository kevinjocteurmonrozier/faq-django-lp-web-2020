from django.core.paginator import Paginator
from django.views.generic import ListView

from app.models import Question


# TODO QUESTIONS PAR DATES, PAR HOT, PAR ALL TIME LIKES


class QuestionListView(ListView):
    template_name = 'index.html'
    model = Question
    paginate_by = 3

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['questions'] = Question.objects.all()
        return result
