from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from app.forms.register_form import RegisterForm


def register_view(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('app_home')
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form': form})
