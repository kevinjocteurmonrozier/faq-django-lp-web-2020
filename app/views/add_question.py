from django.contrib.auth import authenticate
from django.shortcuts import redirect, render
from django.utils.text import slugify

from app.forms.question_form import AddQuestionForm


def add_question_view(request):
    if request.method == 'POST':
        form = AddQuestionForm(request.POST)
        if form.is_valid():
            post = form.save(user=request.user, commit=False)
            post.slug = slugify(post.title)
            post.save()
            title = form.cleaned_data.get('title')
            description = form.cleaned_data.get('description')
            return redirect('app_question_detail', slug=slugify(post.title))
    else:
        form = AddQuestionForm()
    return render(request, 'add_question.html', {'form': form})
