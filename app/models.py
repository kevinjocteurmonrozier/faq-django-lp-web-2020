from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Question(models.Model):
    slug = models.SlugField(default='')
    title = models.CharField(max_length=50, default='')
    description = models.TextField(max_length=2000, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    likes = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default='')

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.title + ' : ' + self.description[:80]


class Comment(models.Model):
    description = models.TextField(max_length=2000, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default='')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, default='')

    class Meta:
        ordering = ['-created_at']



