"""faq_projet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from app.views.add_comment import add_comment_view
from app.views.add_question import add_question_view
from app.views.login import login_view
from app.views.logout import logout_view
from app.views.question_detail import QuestionsDetailView
from app.views.question_list import QuestionListView
from app.views.register import register_view
from app.views.search import QuestionSearchView

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', QuestionListView.as_view(), name='app_home'),
    path('questions/<slug:slug>', QuestionsDetailView.as_view(), name='app_question_detail'),
    path('questions/search/', QuestionSearchView.as_view(), name='app_question_search'),
    path('register/', register_view, name='app_register'),
    path('login/', login_view, name='app_login'),
    path('logout/', logout_view, name='app_logout'),
    path('add/', add_question_view, name='app_add_question'),
    path('questions/<slug:slug>/add-comment/', add_comment_view, name='app_add_comment'),
]
